<?php

namespace Bczopp\SimpleShoppingCart\Controller;

use Bczopp\SimpleShoppingCart\CQ\Command\AddProduct;
use Bczopp\SimpleShoppingCart\CQ\Command\EditOrderedProduct;
use Bczopp\SimpleShoppingCart\CQ\Command\RemoveOrderedProduct;
use Bczopp\SimpleShoppingCart\CQ\CommandHandler\AddProductHandler;
use Bczopp\SimpleShoppingCart\CQ\CommandHandler\EditOrderedProductHandler;
use Bczopp\SimpleShoppingCart\CQ\CommandHandler\RemoveOrderedProductHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;

final class OrderedProductController extends AbstractController
{
    public function __construct(
        private SerializerInterface $serializer,
        private AddProductHandler $addProductHandler,
        private EditOrderedProductHandler $editOrderedProductHandler,
        private RemoveOrderedProductHandler $removeOrderedProductHandler
    ) {}

    private function finalize(mixed $data): JsonResponse
    {
        return JsonResponse::fromJsonString($this->serializer->serialize($data, 'json'));
    }

    public function addProduct(AddProduct $command): JsonResponse
    {
        $id = $this->addProductHandler->handle($command);
        return $this->finalize($id);
    }

    public function editOrderedProduct(EditOrderedProduct $command): JsonResponse
    {
        $cart = $this->editOrderedProductHandler->handle($command);
        return $this->finalize($cart);
    }

    public function removeOrderedProduct(RemoveOrderedProduct $command): JsonResponse
    {
        $deleted = $this->removeOrderedProductHandler->handle($command);
        return $this->finalize($deleted);
    }
}
