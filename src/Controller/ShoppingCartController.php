<?php


namespace Bczopp\SimpleShoppingCart\Controller;

use Bczopp\SimpleShoppingCart\CQ\Command\DeleteCart;
use Bczopp\SimpleShoppingCart\CQ\Command\InitCart;
use Bczopp\SimpleShoppingCart\CQ\CommandHandler\DeleteCartHandler;
use Bczopp\SimpleShoppingCart\CQ\CommandHandler\InitCartHandler;
use Bczopp\SimpleShoppingCart\CQ\Query\ShowCart;
use Bczopp\SimpleShoppingCart\CQ\QueryHandler\ShowCartHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;

final class ShoppingCartController extends AbstractController
{
    public function __construct(
        private SerializerInterface $serializer,
        private InitCartHandler $initCartHandler,
        private ShowCartHandler $showCartHandler,
        private DeleteCartHandler $deleteCartHandler
    ) {}

    private function finalize(mixed $data): JsonResponse
    {
        return JsonResponse::fromJsonString($this->serializer->serialize($data, 'json'));
    }


    public function initCart(InitCart $command): JsonResponse
    {
        $id = $this->initCartHandler->handle($command);
        return $this->finalize($id);
    }

    public function show(ShowCart $query): JsonResponse
    {
        $cart = $this->showCartHandler->handle($query);
        return $this->finalize($cart);
    }

    public function deleteCart(DeleteCart $command): JsonResponse
    {
        $deleted = $this->deleteCartHandler->handle($command);
        return $this->finalize($deleted);
    }
}
