<?php


namespace Bczopp\SimpleShoppingCart\Normalizer;

use Bczopp\SimpleShoppingCart\ValueObject\AbstractId;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class IdNormalizer implements NormalizerInterface
{
    public function normalize($object, string $format = null, array $context = [])
    {
        return $object->getValue();
    }

    public function supportsNormalization($data, string $format = null)
    {
        return $data instanceof AbstractId;
    }
}
