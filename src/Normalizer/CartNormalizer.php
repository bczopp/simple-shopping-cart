<?php


namespace Bczopp\SimpleShoppingCart\Normalizer;

use Bczopp\SimpleShoppingCart\DTO\Cart;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class CartNormalizer implements NormalizerInterface
{
    public function normalize($object, string $format = null, array $context = []): array
    {
        /** @var Cart $object */
        $cart = [
            'meta_data' => $object->getMetaData(),
            'items' => $object->getItems()
        ];
        return $cart;
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof Cart;
    }
}
