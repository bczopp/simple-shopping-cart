<?php

namespace Bczopp\SimpleShoppingCart\Entity;

use Bczopp\SimpleShoppingCart\ValueObject\CartId;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ShoppingCartRepository::class)
 */
final class ShoppingCart
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;
    /**
     * @ORM\Column(type="object")
     */
    private CartId $cartId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCartId(): ?CartId
    {
        return $this->cartId;
    }

    public function setCartId(CartId $cartId): self
    {
        $this->cartId = $cartId;

        return $this;
    }
}
