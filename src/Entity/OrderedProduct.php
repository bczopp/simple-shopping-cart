<?php

namespace Bczopp\SimpleShoppingCart\Entity;

use Bczopp\SimpleShoppingCart\ValueObject\Amount;
use Bczopp\SimpleShoppingCart\ValueObject\CartId;
use Bczopp\SimpleShoppingCart\ValueObject\ProductId;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderedProductRepository::class)
 */
final class OrderedProduct
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;
    /**
     * @ORM\Column(type="object")
     */
    private ProductId $productId;
    /**
     * @ORM\Column(type="object")
     */
    private Amount $amount;
    /**
     * @ORM\Column(type="object")
     */
    private CartId $cartId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductId(): ?ProductId
    {
        return $this->productId;
    }

    public function setProductId(ProductId $productId): self
    {
        $this->productId = $productId;

        return $this;
    }

    public function getAmount(): ?Amount
    {
        return $this->amount;
    }

    public function setAmount(Amount $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCartId(): ?CartId
    {
        return $this->cartId;
    }

    public function setCartId(CartId $cartId): self
    {
        $this->cartId = $cartId;

        return $this;
    }
}
