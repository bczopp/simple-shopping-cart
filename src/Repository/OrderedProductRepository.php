<?php

namespace Bczopp\SimpleShoppingCart\Repository;

use Bczopp\SimpleShoppingCart\CQ\OrderedProductRepositoryInterface;
use Bczopp\SimpleShoppingCart\Entity\OrderedProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;

/**
 * @method OrderedProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderedProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderedProduct[]    findAll()
 * @method OrderedProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class OrderedProductRepository extends ServiceEntityRepository implements OrderedProductRepositoryInterface
{
    private LoggerInterface $logger;

    public function __construct(ManagerRegistry $registry, LoggerInterface $logger)
    {
        parent::__construct($registry, OrderedProduct::class);
        $this->logger = $logger;
    }

    public function insert(OrderedProduct $product): bool
    {
        $this->_em->persist($product);
        $this->_em->flush();
        return true;
    }

    public function update(OrderedProduct $product): bool
    {
        $this->_em->flush();
        return true;
    }

    public function delete(OrderedProduct $product): bool
    {
        $this->_em->remove($product);
        $this->_em->flush();
        return true;
    }
}
