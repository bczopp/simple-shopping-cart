<?php

namespace Bczopp\SimpleShoppingCart\Repository;

use Bczopp\SimpleShoppingCart\CQ\ShoppingCartRepositoryInterface;
use Bczopp\SimpleShoppingCart\Entity\ShoppingCart;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;

/**
 * @method ShoppingCart|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShoppingCart|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShoppingCart[]    findAll()
 * @method ShoppingCart[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class ShoppingCartRepository extends ServiceEntityRepository implements ShoppingCartRepositoryInterface
{
    private LoggerInterface $logger;

    public function __construct(ManagerRegistry $registry, LoggerInterface $logger)
    {
        parent::__construct($registry, ShoppingCart::class);
        $this->logger = $logger;
    }

    public function insert(ShoppingCart $shoppingCart): bool
    {
        $this->_em->persist($shoppingCart);
        $this->_em->flush();
        return true;
    }

    public function delete(ShoppingCart $shoppingCart): bool
    {
        $this->_em->remove($shoppingCart);
        $this->_em->flush();
        return true;
    }
}
