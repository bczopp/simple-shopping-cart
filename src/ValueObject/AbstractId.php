<?php


namespace Bczopp\SimpleShoppingCart\ValueObject;

abstract class AbstractId
{
    public function __construct( private string $value) {}

    public function getValue(): string
    {
        return $this->value;
    }
}
