<?php


namespace Bczopp\SimpleShoppingCart\ValueObject;

final class Amount
{
    public function __construct(private int $value) {}

    public function getValue(): int
    {
        return $this->value;
    }
}
