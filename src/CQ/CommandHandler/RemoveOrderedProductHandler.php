<?php


namespace Bczopp\SimpleShoppingCart\CQ\CommandHandler;

use Bczopp\SimpleShoppingCart\CQ\Command\RemoveOrderedProduct;
use Bczopp\SimpleShoppingCart\CQ\OrderedProductRepositoryInterface;
use Bczopp\SimpleShoppingCart\CQ\ShoppingCartRepositoryInterface;
use Bczopp\SimpleShoppingCart\Entity\OrderedProduct;
use Bczopp\SimpleShoppingCart\Exception\NotFoundException;

final class RemoveOrderedProductHandler
{
    public function __construct(
        private ShoppingCartRepositoryInterface $cartRepo,
        private OrderedProductRepositoryInterface $productRepo
    ){}

    public function handle(RemoveOrderedProduct $command)
    {
        $cartId = $command->getCartId();
        $productId = $command->getProductId();
        $cart = $this->cartRepo->findOneBy(['cartId' => $cartId]);
        if (!$cart) {
            throw new NotFoundException('cart not found');
        }
        /** @var OrderedProduct $product */
        $product = $this->productRepo->findOneBy(['cartId' => $cartId, 'productId' => $productId]);
        if ($product) {
            return $this->productRepo->delete($product);
        }
        return true;
    }
}
