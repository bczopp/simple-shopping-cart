<?php

namespace Bczopp\SimpleShoppingCart\CQ\CommandHandler;

use Bczopp\SimpleShoppingCart\CQ\Command\AddProduct;
use Bczopp\SimpleShoppingCart\CQ\OrderedProductRepositoryInterface;
use Bczopp\SimpleShoppingCart\CQ\ShoppingCartRepositoryInterface;
use Bczopp\SimpleShoppingCart\Entity\OrderedProduct;
use Bczopp\SimpleShoppingCart\Exception\NotFoundException;
use Bczopp\SimpleShoppingCart\Exception\ResourceExistsException;

final class AddProductHandler
{
    public function __construct(
        private ShoppingCartRepositoryInterface $cartRepo,
        private OrderedProductRepositoryInterface $productRepo
    ){}

    public function handle(AddProduct $command): bool
    {
        $cart = $this->cartRepo->findOneBy(['cartId' => $command->getCartId()]);
        if (!$cart) {
            throw new NotFoundException('cart not found');
        }
        $product = $this->productRepo->findOneBy(
            [
                'cartId' => $command->getCartId(),
                'productId' => $command->getProductId()
            ]
        );

        if ($product) {
            throw new ResourceExistsException('product already in cart');
        }

        $product = new OrderedProduct();
        $product->setCartId($command->getCartId());
        $product->setProductId($command->getProductId());
        $product->setAmount($command->getAmount());
        return $this->productRepo->insert($product);
    }
}
