<?php

namespace Bczopp\SimpleShoppingCart\CQ\CommandHandler;

use Bczopp\SimpleShoppingCart\CQ\Command\DeleteCart;
use Bczopp\SimpleShoppingCart\CQ\OrderedProductRepositoryInterface;
use Bczopp\SimpleShoppingCart\CQ\ShoppingCartRepositoryInterface;
use Bczopp\SimpleShoppingCart\Entity\OrderedProduct;
use Bczopp\SimpleShoppingCart\Entity\ShoppingCart;

final class DeleteCartHandler
{
    public function __construct(
        private ShoppingCartRepositoryInterface $cartRepo,
        private OrderedProductRepositoryInterface $productRepo
    ){}

    public function handle(DeleteCart $command): bool
    {
        $cartId = $command->getCartId();

        /** @var OrderedProduct $products */
        $products = $this->productRepo->findBy(['cartId' => $cartId]);
        foreach ($products as $product) {
            $this->productRepo->delete($product);
        }
        /** @var ShoppingCart $cart */
        $cart = $this->cartRepo->findOneBy(['cartId' => $cartId]);
        if ($cart) {
            $this->cartRepo->delete($cart);
        }
        return true;
    }
}
