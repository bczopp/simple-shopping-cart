<?php


namespace Bczopp\SimpleShoppingCart\CQ\CommandHandler;

use Bczopp\SimpleShoppingCart\CQ\Command\InitCart;
use Bczopp\SimpleShoppingCart\CQ\ShoppingCartRepositoryInterface;
use Bczopp\SimpleShoppingCart\Entity\ShoppingCart;
use Bczopp\SimpleShoppingCart\ValueObject\CartId;
use Ramsey\Uuid\Uuid;

final class InitCartHandler
{
    public function __construct(
        private ShoppingCartRepositoryInterface $repo
    ){}

    public function handle(InitCart $query): ?CartId
    {
        $id = new CartId(Uuid::uuid4());
        $shoppingCart = new ShoppingCart();
        $shoppingCart->setCartId($id);
        if ($this->repo->insert($shoppingCart)) {
            return $id;
        }
        return null;
    }
}
