<?php


namespace Bczopp\SimpleShoppingCart\CQ\CommandHandler;

use Bczopp\SimpleShoppingCart\CQ\Command\EditOrderedProduct;
use Bczopp\SimpleShoppingCart\CQ\OrderedProductRepositoryInterface;
use Bczopp\SimpleShoppingCart\CQ\ShoppingCartRepositoryInterface;
use Bczopp\SimpleShoppingCart\Entity\OrderedProduct;
use Bczopp\SimpleShoppingCart\Exception\NotFoundException;

final class EditOrderedProductHandler
{
    public function __construct(
        private ShoppingCartRepositoryInterface $cartRepo,
        private OrderedProductRepositoryInterface $productRepo
    ){}

    public function handle(EditOrderedProduct $command): bool
    {
        $cart = $this->cartRepo->findOneBy(['cartId' => $command->getCartId()]);
        if (!$cart) {
            throw new NotFoundException('cart not found');
        }
        /** @var OrderedProduct $orderedProduct */
        $orderedProduct = $this->productRepo->findOneBy(
            [
                'cartId' => $command->getCartId(),
                'productId' => $command->getProductId()
            ]
        );
        if (!$orderedProduct) {
            throw new NotFoundException('product not found in list');
        }
        $orderedProduct->setAmount($command->getAmount());
        return $this->productRepo->update($orderedProduct);
    }
}
