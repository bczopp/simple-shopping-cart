<?php


namespace Bczopp\SimpleShoppingCart\CQ;

use Bczopp\SimpleShoppingCart\Entity\OrderedProduct;
use Bczopp\SimpleShoppingCart\Entity\ShoppingCart;
use Doctrine\Persistence\ObjectRepository;

interface ShoppingCartRepositoryInterface extends ObjectRepository
{
    public function insert(ShoppingCart $shoppingCart): bool;
    public function delete(ShoppingCart $shoppingCart): bool;
}
