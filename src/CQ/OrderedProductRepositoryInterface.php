<?php


namespace Bczopp\SimpleShoppingCart\CQ;

use Bczopp\SimpleShoppingCart\Entity\OrderedProduct;
use Doctrine\Persistence\ObjectRepository;

interface OrderedProductRepositoryInterface extends ObjectRepository
{
    public function insert(OrderedProduct $product): bool;
    public function update(OrderedProduct $product): bool;
    public function delete(OrderedProduct $product): bool;
}
