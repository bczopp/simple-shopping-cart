<?php


namespace Bczopp\SimpleShoppingCart\CQ\Command;

use Bczopp\SimpleShoppingCart\ValueObject\CartId;

final class DeleteCart
{
    public function __construct(private CartId $cartId){}

    public function getCartId(): CartId
    {
        return $this->cartId;
    }
}
