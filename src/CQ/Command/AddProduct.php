<?php


namespace Bczopp\SimpleShoppingCart\CQ\Command;

use Bczopp\SimpleShoppingCart\ValueObject\Amount;
use Bczopp\SimpleShoppingCart\ValueObject\CartId;
use Bczopp\SimpleShoppingCart\ValueObject\ProductId;

final class AddProduct
{
    public function __construct(
        private CartId $cartId,
        private ProductId $productId,
        private Amount $amount
    ){}

    public function getCartId(): CartId
    {
        return $this->cartId;
    }

    public function getProductId(): ProductId
    {
        return $this->productId;
    }

    public function getAmount(): Amount
    {
        return $this->amount;
    }
}
