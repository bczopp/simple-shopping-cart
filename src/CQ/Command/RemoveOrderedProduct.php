<?php


namespace Bczopp\SimpleShoppingCart\CQ\Command;

use Bczopp\SimpleShoppingCart\ValueObject\CartId;
use Bczopp\SimpleShoppingCart\ValueObject\ProductId;

final class RemoveOrderedProduct
{
    public function __construct(
        private CartId $cartId,
        private ProductId $productId
    ){}

    public function getCartId(): CartId
    {
        return $this->cartId;
    }

    public function getProductId(): ProductId
    {
        return $this->productId;
    }
}
