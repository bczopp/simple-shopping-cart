<?php


namespace Bczopp\SimpleShoppingCart\CQ\Query;

use Bczopp\SimpleShoppingCart\ValueObject\CartId;

final class ShowCart
{
    public function __construct(private CartId $cartId)
    {}

    public function getCartId(): CartId
    {
        return $this->cartId;
    }
}
