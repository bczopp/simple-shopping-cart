<?php


namespace Bczopp\SimpleShoppingCart\CQ\QueryHandler;

use Bczopp\SimpleShoppingCart\CQ\OrderedProductRepositoryInterface;
use Bczopp\SimpleShoppingCart\CQ\Query\ShowCart;
use Bczopp\SimpleShoppingCart\CQ\ShoppingCartRepositoryInterface;
use Bczopp\SimpleShoppingCart\DTO\Cart;
use Bczopp\SimpleShoppingCart\Entity\ShoppingCart;
use Bczopp\SimpleShoppingCart\Exception\NotFoundException;
use Bczopp\SimpleShoppingCart\Services\Converter\CartConverter;

final class ShowCartHandler
{
    public function __construct(
        private ShoppingCartRepositoryInterface $cartRepo,
        private OrderedProductRepositoryInterface $productRepo,
        private CartConverter $converter
    ) {}

    public function handle(ShowCart $query): ?Cart
    {
        $cartId = $query->getCartId();
        /** @var ShoppingCart $shoppingCart */
        $shoppingCart = $this->cartRepo->findOneBy(['cartId' => $cartId]);
        if (!$shoppingCart) {
            throw new NotFoundException('no cart found');
        }
        $orderedProducts = $this->productRepo->findBy(['cartId' => $cartId]) ?? [];
        return $this->converter->convertFrom($shoppingCart, $orderedProducts);
    }
}
