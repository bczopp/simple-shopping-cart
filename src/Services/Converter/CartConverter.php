<?php


namespace Bczopp\SimpleShoppingCart\Services\Converter;

use Bczopp\SimpleShoppingCart\DTO\Cart;
use Bczopp\SimpleShoppingCart\Entity\OrderedProduct;
use Bczopp\SimpleShoppingCart\Entity\ShoppingCart;

final class CartConverter
{
    public function convertFrom(?ShoppingCart $shoppingCart, array $products): Cart
    {
        return new Cart(
            $this->convertCartToDTOMetaData($shoppingCart),
            $this->convertOrderedProductsToDTOProducts($products)
        );
    }

    private function convertCartToDTOMetaData(?ShoppingCart $shoppingCart): array
    {
        return [
            'cart_id' => $shoppingCart->getCartId()->getValue()
        ];
    }

    private function convertOrderedProductsToDTOProducts(array $orderedProducts): array
    {
        return array_map(
            fn(OrderedProduct $product) =>  [
                    'product_id' => $product->getProductId()->getValue(),
                    'amount' => $product->getAmount()->getValue()
                ],
            $orderedProducts
        );
    }
}
