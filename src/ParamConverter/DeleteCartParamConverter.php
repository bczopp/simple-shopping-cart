<?php


namespace Bczopp\SimpleShoppingCart\ParamConverter;

use Bczopp\SimpleShoppingCart\CQ\Command\DeleteCart;
use Bczopp\SimpleShoppingCart\ValueObject\CartId;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

final class DeleteCartParamConverter implements ParamConverterInterface
{
    public function apply(Request $request, ParamConverter $configuration): void
    {
        $id = $request->get('cartId');
        $cartId = new CartId($id);
        $command = new DeleteCart($cartId);
        $request->attributes->add(['command' => $command]);
    }

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === DeleteCart::class;
    }
}
