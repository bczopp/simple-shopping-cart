<?php


namespace Bczopp\SimpleShoppingCart\ParamConverter;

use Bczopp\SimpleShoppingCart\CQ\Command\InitCart;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

final class InitCartParamConverter implements ParamConverterInterface
{
    public function apply(Request $request, ParamConverter $configuration): void
    {
        $command = new InitCart();
        $request->attributes->add(['command' => $command]);
    }

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === InitCart::class;
    }
}
