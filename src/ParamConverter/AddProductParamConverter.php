<?php


namespace Bczopp\SimpleShoppingCart\ParamConverter;

use Bczopp\SimpleShoppingCart\CQ\Command\AddProduct;
use Bczopp\SimpleShoppingCart\CQ\Command\DeleteCart;
use Bczopp\SimpleShoppingCart\Exception\BadRequestException;
use Bczopp\SimpleShoppingCart\ValueObject\Amount;
use Bczopp\SimpleShoppingCart\ValueObject\CartId;
use Bczopp\SimpleShoppingCart\ValueObject\ProductId;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

final class AddProductParamConverter implements ParamConverterInterface
{
    public function apply(Request $request, ParamConverter $configuration): void
    {
        $cartId = new CartId($request->get('cartId'));
        if (!Uuid::isValid($cartId->getValue())) {
            throw new BadRequestException('invalid cart id');
        }
        $productId = new ProductId($request->get('productId'));
        $amount = new Amount($request->get('amount'));

        $command = new AddProduct($cartId, $productId, $amount);
        $request->attributes->add(['command' => $command]);
    }

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === AddProduct::class;
    }
}
