<?php


namespace Bczopp\SimpleShoppingCart\ParamConverter;

use Bczopp\SimpleShoppingCart\CQ\Query\ShowCart;
use Bczopp\SimpleShoppingCart\Exception\BadRequestException;
use Bczopp\SimpleShoppingCart\ValueObject\CartId;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

final class ShowCartParamConverter implements ParamConverterInterface
{
    public function apply(Request $request, ParamConverter $configuration): void
    {
        $id = $request->get('cartId');
        if (!Uuid::isValid($id)) {
            throw new BadRequestException('invalid cart id');
        }
        $query = new ShowCart(new CartId($id));
        $request->attributes->add(['query' => $query]);
    }

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === ShowCart::class;
    }
}
