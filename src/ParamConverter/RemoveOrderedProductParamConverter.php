<?php


namespace Bczopp\SimpleShoppingCart\ParamConverter;

use Bczopp\SimpleShoppingCart\CQ\Command\AddProduct;
use Bczopp\SimpleShoppingCart\CQ\Command\DeleteCart;
use Bczopp\SimpleShoppingCart\CQ\Command\RemoveOrderedProduct;
use Bczopp\SimpleShoppingCart\ValueObject\Amount;
use Bczopp\SimpleShoppingCart\ValueObject\CartId;
use Bczopp\SimpleShoppingCart\ValueObject\ProductId;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

final class RemoveOrderedProductParamConverter implements ParamConverterInterface
{
    public function apply(Request $request, ParamConverter $configuration): void
    {
        $cartId = new CartId($request->get('cartId'));
        $productId = new ProductId($request->get('productId'));

        $command = new RemoveOrderedProduct($cartId, $productId);
        $request->attributes->add(['command' => $command]);
    }

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === RemoveOrderedProduct::class;
    }
}
