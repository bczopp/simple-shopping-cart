<?php


namespace Bczopp\SimpleShoppingCart\DTO;

final class Cart
{
    public function __construct(
        private array $metaData,
        private array $items
    ) {}

    public function getMetaData(): array
    {
        return $this->metaData;
    }

    public function getItems(): array
    {
        return $this->items;
    }
}
