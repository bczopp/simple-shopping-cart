simple-shopping-cart
===========

Author: Benno Czopp

Projektziel
----------

Ein einfaches Programm, das mehrere Warenkörbe mit Produkten verwalten kann.

Es sollen Produkte hinzugefügt, bearbeitet und entfernt werden können. Des Weiteren soll man eine Liste der im Warenkorb befindlichen Produkte aufrufen können.

Das Programm soll über eine RESTful API gesteuert werden können.


ProjektBeschreibung
---------------

Das Projekt wurde umgesetzt in PHP 8.0 mit dem Symfony 5.2 Framework.
Es wurde auf __SOLID__ und __clean code__ geachtet. Außerdem wurden Verantwortlichkeiten durch CQRS aufgeteilt.

Als Ressourcen wurden __ShoppingCart__ und __OrderedProduct__ definiert, welche über eigene Repositories verwaltet werden. Persistiert werden diese Daten in einer Sqlite Datenbank.

Für die API ist eine Versionierung vorgesehen. daher haben alle Routen das Prefix ___/api/v1___.
Die API bietet folgende Routen zur Steuerung bereit:

__/api/v1/carts/{cartId}__

- POST
- Rückgabe: Id des Warenkorbs
- Ein Warenkorb muss initialisiert werden, bevor er genutzt werden kann

__/api/v1/carts/{cartId}__
 
- GET
- Rückgabe: Warenkorb mit MetaDaten und Produkten
- sofern vorhanden werden Informationen zu dem Warenkorb und den darin befindlichen Produkten ausgegeben.

__/api/v1/carts/{cartId}__

- DELETE
- Rückgabe: bool
- der Warenkorb und alle seine Produkte werden gelöscht

__/api/v1/carts/{cartId}/products/{productId}/amount/{amount}__

- POST
- Rückgabe: bool
- ein neues Produkt wird dem Warenkorb hinzugefügt. NUR, wenn das Produkt noch nicht im Warenkorb existiert.

__/api/v1/carts/{cartId}/products/{productId}/amount/{amount}__

- PUT
- Rückgabe: bool
- ein existierendes Produkt wird verändert, in diesem Fall die bestellte Anzahl des Produkts. Weitere Routen sind aktuell nicht implementiert.

__/api/v1/carts/{cartId}/products/{productId}__

- DELETE
- Rückgabe: bool
- ein existierendes Produkt wird aus dem Warenkorb entfernt

Installation
----------

Vorbereitend:

Das Programm wurde auf einem LinuxOS erstellt. Eventuell abweichendes Verhalten anderer OS wurden nicht beachtet.

Da das Projekt in Entwicklung ist, gibt es noch keine Migrationen.

Für die Entwicklung verwendete Tools:
- __Symfony CLI Tool__ (https://symfony.com/download) wurde verwendet, um lokal einen PHP Server zu starten.  
- __direnv__ damit lokal die Umgebungsvariablen geladen werden.
- __Composer__ in Version >= 2
- __PHP__ in Version >= 8.0
- __Symfony__ in Version >=5.2

Projekt:

- __Repo__ klonen und in das Verzeichnis wechseln

Der einfachste Weg, wenn keine weiteren Anpassungen erfolgen müssen ist:
- ___composer bootstrap-envrc___
- ___composer bootstrap-system___
- ___symfony server:start --no-tls___ (es empfiehlt sich ein separater Tab)

Dieses Script erledigt alle nötigen Aufgaben um das Programm (mit den o.g. Tools) lauffähig zu machen.

-----------------

Alternativ kann man auch den manuellen Weg gehen, der in diesem Fall so aussieht:

- ___cp .envrc.dist .envrc___ (kann angepasst werden)
- ___direnv allow___
  

- ___composer install___
- ___codecept build___
- ___mkdir var/db___
- ___touch var/db/data.sqlite___
- ___console doctrine:database:create___
- ___console doctrine:schema:update --force___
  

- ___symfony server:start --no-tls___ (es empfiehlt sich en separater Tab)

--------------------------

das Programm sollte jetzt über __http://localhost:8000/<entsprechendeRoute>__ (_möglicherweise OS abhängig, das Tool sagt einem die URL_) erreichbar sein

Testen:

- durch die _.envrc_ ist der Befehl __codecept__ bereits im Root-Verzeichnis des Projektes ausführbar. Wäre dies nicht der Fall, kann der Befehl über __vendor/bin/codecept__ ausgeführt werden.
- __codecept run \<optional suite-name> \<optional test-class>:\<optional testname>__
- aktuell implementierte suites: _api_ und _functional_
- es können alle Tests auf einmal, einzelne Suits, eine einzelne TestKlasse oder sogar einzelne Tests einer Klasse gestartet werden
- durch Erweitern mit diesen Parametern __--coverage --coverage-html__ wird nach Beenden der Tests ein Bericht zur CodeCoverage der Dateien im _src/_-Verzeichnis erstellt. dieser ist zu finden unter _tests/\_output/api.remote.coverage/index.html_ und kann im Browser geöffnet werden.
- die __Coverage__, die über die ApiTests (\_output/api.remote.coverage) ermittelt wird, beträgt (zeilenweise) +95 %.
- die __Coverage__, die über die FunctionalTests (\_output/coverage) ermittelt wird, liegt mit knapp über 60 % stark darunter. Das liegt daran, dass Controller, ParamConverter und Normalizer gar nicht durchlaufen werden. Die werden erst bei den ApiTests aufgerufen.

TroubleShooting / Refactoring
---------

- es existiert aktuell keine Security. Theoretisch könnte jeder den Service aufrufen und sich ShoppingCarts erstellen. Mittels Autorisierung könnte dies geregelt werden.
- das Projekt wurde in lokaler Umgebung entwickelt. Auf lange Sicht wäre ein Umbau auf Docker ratsam.
- Die Testdaten werden für jeden Test in dem entsprechenden Test je nach Bedarf erstellt. Das sollte durch Fixtures ersetzt werden. Außerdem sollte eine weitere Datenbank für ProduktivDaten angelegt und das System entsprechend konfiguriert werden.
- Ein ExceptionHandler muss für __dev__ bzw __prod__ Umgebung definiert werden, der die aus Exceptions resultierenden ___Internal Server Error___ in ___Bad Request___ wandelt
- ___composer bootstrap-system___ bzw die manuelle Installation verwendet den Befehl ___console doctrine:schema:update --force___. Dies ist aktuell nur legitim, da das Projekt nicht released wurde. Ab dem Release darf nur noch mittels Migrationen etwas an der Datenbank-Struktur geändert werden.