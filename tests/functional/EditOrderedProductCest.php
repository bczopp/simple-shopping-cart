<?php


namespace Bczopp\SimpleShoppingCart\Tests\functional;

use Assert\Assertion;
use Assert\AssertionFailedException;
use Bczopp\SimpleShoppingCart\CQ\Command\AddProduct;
use Bczopp\SimpleShoppingCart\CQ\Command\EditOrderedProduct;
use Bczopp\SimpleShoppingCart\CQ\CommandHandler\AddProductHandler;
use Bczopp\SimpleShoppingCart\CQ\CommandHandler\EditOrderedProductHandler;
use Bczopp\SimpleShoppingCart\CQ\OrderedProductRepositoryInterface;
use Bczopp\SimpleShoppingCart\Entity\OrderedProduct;
use Bczopp\SimpleShoppingCart\Exception\NotFoundException;
use Bczopp\SimpleShoppingCart\Repository\OrderedProductRepository;
use Bczopp\SimpleShoppingCart\ValueObject\Amount;
use Bczopp\SimpleShoppingCart\ValueObject\CartId;
use Bczopp\SimpleShoppingCart\ValueObject\ProductId;
use Ramsey\Uuid\Uuid;

class EditOrderedProductCest
{
    private EditOrderedProductHandler $handler;
    private OrderedProductRepositoryInterface $productRepo;

    public function _before(\FunctionalTester $I)
    {
        $I->cleanCartRepository();
        $I->cleanProductRepository();

        $this->handler = $I->grabService(EditOrderedProductHandler::class);
        $this->productRepo = $I->grabService(OrderedProductRepositoryInterface::class);
    }

    public function failOnNonExistingCart(\FunctionalTester $I)
    {
        $I->expectThrowable(
            NotFoundException::class,
            fn() => $this->handler->handle(new EditOrderedProduct(
                    new CartId(Uuid::uuid4()->toString()),
                    new ProductId('asd'),
                    new Amount(1)
                ))
        );
    }

    public function successOnExistingProduct(\FunctionalTester $I)
    {
        $cartId = $I->generateShoppingCart();
        $productId = new ProductId('testId');
        $amount = new Amount(1);
        $products = [
            ['product_id'=>$productId, 'amount'=>$amount],
        ];
        $I->addProductsToCart($cartId, $products);

        $product = $this->productRepo->findOneBy(['cartId'=>$cartId,'productId'=>$productId]);
        $I->assertInstanceOf(OrderedProduct::class, $product);
        $I->assertEquals($amount, $product->getAmount());
        $I->assertEquals($cartId, $product->getCartId());
        $editedAmount = new Amount(3);
        $command = new EditOrderedProduct(
            $cartId,
            $productId,
            $editedAmount
        );
        $saved = $this->handler->handle($command);
        $I->assertTrue($saved);
        $product = $this->productRepo->findOneBy(['cartId'=>$cartId,'productId'=>$productId]);
        $I->assertInstanceOf(OrderedProduct::class, $product);
        $I->assertEquals($editedAmount, $product->getAmount());
    }

    public function failOnNonExistingProduct(\FunctionalTester $I)
    {
        $cartId = $I->generateShoppingCart();
        $productId = new ProductId('testId');
        $amount = new Amount(3);
        $command = new EditOrderedProduct(
            $cartId,
            $productId,
            $amount
        );
        $I->expectThrowable(
            NotFoundException::class,
            fn() => $this->handler->handle($command)
        );
    }
}
