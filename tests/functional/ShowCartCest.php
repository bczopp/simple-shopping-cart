<?php


namespace Bczopp\SimpleShoppingCart\Tests\functional;

use Bczopp\SimpleShoppingCart\CQ\OrderedProductRepositoryInterface;
use Bczopp\SimpleShoppingCart\CQ\Query\ShowCart;
use Bczopp\SimpleShoppingCart\CQ\QueryHandler\ShowCartHandler;
use Bczopp\SimpleShoppingCart\CQ\ShoppingCartRepositoryInterface;
use Bczopp\SimpleShoppingCart\DTO\Cart;
use Bczopp\SimpleShoppingCart\Exception\NotFoundException;
use Bczopp\SimpleShoppingCart\ValueObject\Amount;
use Bczopp\SimpleShoppingCart\ValueObject\CartId;
use Bczopp\SimpleShoppingCart\ValueObject\ProductId;
use Ramsey\Uuid\Uuid;

class ShowCartCest
{
    private ShowCartHandler $handler;
    private ShoppingCartRepositoryInterface $cartRepo;
    private OrderedProductRepositoryInterface $productRepo;

    public function _before(\FunctionalTester $I)
    {
        $I->cleanCartRepository();
        $I->cleanProductRepository();

        $this->handler = $I->grabService(ShowCartHandler::class);
        $this->cartRepo = $I->grabService(ShoppingCartRepositoryInterface::class);
        $this->productRepo = $I->grabService(OrderedProductRepositoryInterface::class);
    }

    public function NotFoundOnWrongIdFormat(\FunctionalTester $I)
    {
        $I->expectThrowable(
            NotFoundException::class,
            fn() => $this->handler->handle(new ShowCart(new CartId('asdasd')))
        );
    }
    public function notFoundOnUnknownId(\FunctionalTester $I)
    {
        $I->expectThrowable(
            NotFoundException::class,
            fn() => $this->handler->handle(new ShowCart(new CartId(Uuid::uuid4())))
        );
    }

    public function findCartWithoutProducts(\FunctionalTester $I)
    {
        $cartId = $I->generateShoppingCart();
        $result = $this->handler->handle(new ShowCart($cartId));
        $I->assertInstanceOf(Cart::class, $result);
        $I->assertEquals(new Cart(['cart_id'=>$cartId->getValue()], []), $result);
    }

    public function findCarWithProducts(\FunctionalTester $I)
    {
        $cartId = $I->generateShoppingCart();
        $metaData = [
            'cart_id' => $cartId->getValue()
        ];
        $products = [
            ['product_id'=>new ProductId('test1'), 'amount'=>new Amount(1)],
            ['product_id'=>new ProductId('test2'), 'amount'=>new Amount(2)],
        ];
        $I->addProductsToCart($cartId, $products);
        $result = $this->handler->handle(new ShowCart($cartId));
        $I->assertInstanceOf(Cart::class, $result);
        $products = array_map(
            fn($product) => [
                'product_id'=>$product['product_id']->getValue(),
                'amount'=>$product['amount']->getValue()
            ],
            $products
        );
        $I->assertEquals(new Cart($metaData, $products), $result);
    }
}
