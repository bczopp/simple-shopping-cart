<?php


namespace Bczopp\SimpleShoppingCart\Tests\functional;

use Assert\AssertionFailedException;
use Bczopp\SimpleShoppingCart\CQ\Command\RemoveOrderedProduct;
use Bczopp\SimpleShoppingCart\CQ\CommandHandler\RemoveOrderedProductHandler;
use Bczopp\SimpleShoppingCart\CQ\OrderedProductRepositoryInterface;
use Bczopp\SimpleShoppingCart\CQ\QueryHandler\ShowCartHandler;
use Bczopp\SimpleShoppingCart\CQ\ShoppingCartRepositoryInterface;
use Bczopp\SimpleShoppingCart\Entity\OrderedProduct;
use Bczopp\SimpleShoppingCart\Exception\NotFoundException;
use Bczopp\SimpleShoppingCart\Repository\OrderedProductRepository;
use Bczopp\SimpleShoppingCart\Repository\ShoppingCartRepository;
use Bczopp\SimpleShoppingCart\ValueObject\Amount;
use Bczopp\SimpleShoppingCart\ValueObject\CartId;
use Bczopp\SimpleShoppingCart\ValueObject\ProductId;
use Ramsey\Uuid\Uuid;

class RemoveOrderedProductCest
{
    private RemoveOrderedProductHandler $handler;
    private ShoppingCartRepositoryInterface $cartRepo;
    private OrderedProductRepositoryInterface $productRepo;

    public function _before(\FunctionalTester $I)
    {
        $I->cleanCartRepository();
        $I->cleanProductRepository();

        $this->handler = $I->grabService(RemoveOrderedProductHandler::class);
        $this->cartRepo = $I->grabService(ShoppingCartRepositoryInterface::class);
        $this->productRepo = $I->grabService(OrderedProductRepositoryInterface::class);
    }

    public function failOnUnknownCart(\FunctionalTester $I)
    {
        $I->expectThrowable(
            NotFoundException::class,
            fn() => $this->handler->handle(new RemoveOrderedProduct(
                    new CartId(Uuid::uuid4()->toString()),
                    new ProductId(Uuid::uuid4()->toString())
                ))
        );

        $I->expectThrowable(
            NotFoundException::class,
            fn() => $this->handler->handle(new RemoveOrderedProduct(
                    new CartId(Uuid::uuid4()->toString()),
                    new ProductId('someId')
                ))
        );
    }

    public function noChangeIfProductIsNotInList(\FunctionalTester $I)
    {
        $cartId = $I->generateShoppingCart();
        $products = [
            ['product_id' => new ProductId('p111'), 'amount' => new Amount(2)]
        ];
        $I->addProductsToCart($cartId, $products);

        $cart = $this->cartRepo->findOneBy(['cartId' => $cartId]);
        $products = $this->productRepo->findBy(['cartId' => $cartId]);

        $this->handler->handle(new RemoveOrderedProduct($cartId, new ProductId('wrongId')));

        $I->assertEquals(
            $cart,
            $this->cartRepo->findOneBy(['cartId' => $cartId])
        );
        $I->assertEquals(
            $products,
            $this->productRepo->findBy(['cartId' => $cartId])
        );
    }
    public function removeItem(\FunctionalTester $I)
    {
        $cartId = $I->generateShoppingCart();
        $productId = new ProductId('p222');
        $products = [
            ['product_id'=>new ProductId('p111'),'amount'=>new Amount(2)],
            ['product_id'=>$productId,'amount'=>new Amount(1)]
        ];
        $I->addProductsToCart($cartId, $products);

        $cart = $this->cartRepo->findOneBy(['cartId'=>$cartId]);
        $products = array_filter(
            $this->productRepo->findBy(['cartId' => $cartId]),
            fn (OrderedProduct $product) => $product->getProductId() !== $productId
        );

        $this->handler->handle(new RemoveOrderedProduct($cartId, $productId));

        $I->assertEquals(
            $cart,
            $this->cartRepo->findOneBy(['cartId'=>$cartId])
        );
        $I->assertEquals(
            $products,
            $this->productRepo->findBy(['cartId' => $cartId])
        );
    }
}
