<?php


namespace Bczopp\SimpleShoppingCart\Tests\functional;

use Assert\Assertion;
use Assert\AssertionFailedException;
use Bczopp\SimpleShoppingCart\CQ\Command\AddProduct;
use Bczopp\SimpleShoppingCart\CQ\CommandHandler\AddProductHandler;
use Bczopp\SimpleShoppingCart\CQ\OrderedProductRepositoryInterface;
use Bczopp\SimpleShoppingCart\Entity\OrderedProduct;
use Bczopp\SimpleShoppingCart\Exception\NotFoundException;
use Bczopp\SimpleShoppingCart\Exception\ResourceExistsException;
use Bczopp\SimpleShoppingCart\Repository\OrderedProductRepository;
use Bczopp\SimpleShoppingCart\ValueObject\Amount;
use Bczopp\SimpleShoppingCart\ValueObject\CartId;
use Bczopp\SimpleShoppingCart\ValueObject\ProductId;
use Ramsey\Uuid\Uuid;

class AddProductCest
{
    private AddProductHandler $handler;
    private OrderedProductRepositoryInterface $productRepo;

    public function _before(\FunctionalTester $I)
    {
        $I->cleanCartRepository();
        $I->cleanProductRepository();

        $this->handler = $I->grabService(AddProductHandler::class);
        $this->productRepo = $I->grabService(OrderedProductRepositoryInterface::class);
    }

    public function failOnNonExistingCart(\FunctionalTester $I)
    {
        $I->expectThrowable(
            NotFoundException::class,
            fn() => $this->handler->handle(new AddProduct(
                    new CartId(Uuid::uuid4()->toString()),
                    new ProductId('asd'),
                    new Amount(1)
                ))
        );
    }

    public function successOnNewProduct(\FunctionalTester $I)
    {
        $cartId = $I->generateShoppingCart();
        $productId = new ProductId('testId');
        $amount = new Amount(1);
        $product = $this->productRepo->findOneBy(['cartId'=>$cartId,'productId'=>$productId]);
        $I->assertEmpty($product);
        $command = new AddProduct(
            $cartId,
            $productId,
            $amount
        );
        $saved = $this->handler->handle($command);
        $I->assertTrue($saved);
        $product = $this->productRepo->findOneBy(['cartId'=>$cartId,'productId'=>$productId]);
        $I->assertInstanceOf(OrderedProduct::class, $product);
        $I->assertEquals($amount, $product->getAmount());
    }

    public function failOnAddingToExistingProduct(\FunctionalTester $I)
    {
        $cartId = $I->generateShoppingCart();
        $productId = new ProductId('testId');
        $amount = new Amount(1);
        $products = [
            ['product_id' => $productId, 'amount' => $amount]
        ];
        $I->addProductsToCart($cartId, $products);

        $I->expectThrowable(
            ResourceExistsException::class,
            fn() => $this->handler->handle(new AddProduct(
                    $cartId,
                    $productId,
                    $amount
                ))
        );
    }
}
