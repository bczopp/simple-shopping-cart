<?php


namespace Bczopp\SimpleShoppingCart\Tests\functional;

use Bczopp\SimpleShoppingCart\CQ\Command\InitCart;
use Bczopp\SimpleShoppingCart\CQ\CommandHandler\InitCartHandler;
use Bczopp\SimpleShoppingCart\CQ\ShoppingCartRepositoryInterface;
use Bczopp\SimpleShoppingCart\Repository\ShoppingCartRepository;

class InitCartCest
{
    private InitCartHandler $handler;
    private ShoppingCartRepositoryInterface $repo;

    public function _before(\FunctionalTester $I)
    {
        $I->cleanCartRepository();
        $this->handler = $I->grabService(InitCartHandler::class);
        $this->repo = $I->grabService(ShoppingCartRepositoryInterface::class);
    }

    public function _after(\FunctionalTester $I)
    {
    }

    public function init(\FunctionalTester $I)
    {
        $I->assertEmpty($this->repo->findAll());
        $cartId = $this->handler->handle(new InitCart());
        $all = $this->repo->findAll();
        $I->assertCount(1, $all);
        $cart = current($all);
        $I->assertEquals($cart->getCartId(), $cartId);
        $I->assertEquals($cart, $this->repo->findOneBy(['cartId'=>$cartId]));
    }
}
