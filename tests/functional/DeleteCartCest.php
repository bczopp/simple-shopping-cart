<?php


namespace Bczopp\SimpleShoppingCart\Tests\functional;

use Bczopp\SimpleShoppingCart\CQ\Command\DeleteCart;
use Bczopp\SimpleShoppingCart\CQ\CommandHandler\DeleteCartHandler;
use Bczopp\SimpleShoppingCart\ValueObject\Amount;
use Bczopp\SimpleShoppingCart\ValueObject\CartId;
use Bczopp\SimpleShoppingCart\ValueObject\ProductId;
use Ramsey\Uuid\Uuid;

class DeleteCartCest
{
    private DeleteCartHandler $handler;

    public function _before(\FunctionalTester $I)
    {
        $I->cleanCartRepository();
        $I->cleanProductRepository();

        $this->handler = $I->grabService(DeleteCartHandler::class);
    }


    public function successOnCartWithoutProducts(\FunctionalTester $I)
    {
        $cartId = $I->generateShoppingCart();

        $success = $this->handler->handle(new DeleteCart($cartId));
        $I->assertTrue($success);

        $I->assertFalse($I->checkIfRepoHasCart($cartId));
        $I->assertFalse($I->checkIfRepoHasAnyProducts($cartId));
    }

    public function successOnCartWithProducts(\FunctionalTester $I)
    {
        $cartId = $I->generateShoppingCart();
        $products = [
            ['product_id'=>new ProductId('test1'), 'amount'=>new Amount(1)],
            ['product_id'=>new ProductId('test2'), 'amount'=>new Amount(2)],
        ];
        $I->addProductsToCart($cartId, $products);

        $success = $this->handler->handle(new DeleteCart($cartId));
        $I->assertTrue($success);

        $I->assertFalse($I->checkIfRepoHasCart($cartId));
        $I->assertFalse($I->checkIfRepoHasAnyProducts($cartId));
    }

    public function successOnNonExistingCartButProducts(\FunctionalTester $I)
    {
        $cartId = new CartId(Uuid::uuid4()->toString());
        $products = [
            ['product_id'=>new ProductId('test1'), 'amount'=>new Amount(1)],
            ['product_id'=>new ProductId('test2'), 'amount'=>new Amount(2)],
        ];
        $I->addProductsToCart($cartId, $products);

        $success = $this->handler->handle(new DeleteCart($cartId));
        $I->assertTrue($success);

        $I->assertFalse($I->checkIfRepoHasCart($cartId));
        $I->assertFalse($I->checkIfRepoHasAnyProducts($cartId));
    }

    public function successOnNonExistingCartAndNoProducts(\FunctionalTester $I)
    {
        $cartId = new CartId(Uuid::uuid4()->toString());

        $success = $this->handler->handle(new DeleteCart($cartId));
        $I->assertTrue($success);

        $I->assertFalse($I->checkIfRepoHasCart($cartId));
        $I->assertFalse($I->checkIfRepoHasAnyProducts($cartId));
    }
}
