<?php
namespace Helper;

use Bczopp\SimpleShoppingCart\Tests\_support\Helper\RepositoryActions;

class Api extends \Codeception\Module
{
    use RepositoryActions;
}
