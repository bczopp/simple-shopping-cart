<?php
namespace Helper;

use Bczopp\SimpleShoppingCart\Tests\_support\Helper\RepositoryActions;

class Functional extends \Codeception\Module
{
    use RepositoryActions;
}
