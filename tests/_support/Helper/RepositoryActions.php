<?php


namespace Bczopp\SimpleShoppingCart\Tests\_support\Helper;

use Bczopp\SimpleShoppingCart\CQ\OrderedProductRepositoryInterface;
use Bczopp\SimpleShoppingCart\CQ\ShoppingCartRepositoryInterface;
use Bczopp\SimpleShoppingCart\Entity\OrderedProduct;
use Bczopp\SimpleShoppingCart\Entity\ShoppingCart;
use Bczopp\SimpleShoppingCart\ValueObject\CartId;
use Bczopp\SimpleShoppingCart\ValueObject\ProductId;
use Ramsey\Uuid\Uuid;

/**
 * Trait RepositoryActions
 *
 * zugegeben, das sollte so nicht sein. Besser wären Fixtures.
 *
 * @package Bczopp\SimpleShoppingCart\Tests\_support\Helper
 */
trait RepositoryActions
{
    abstract protected function getModule($name);

    public function cleanCartRepository(): void
    {
        /** @var ShoppingCartRepositoryInterface $repo */
        $repo = $this->getModule('Symfony')->grabService(ShoppingCartRepositoryInterface::class);
        foreach ($repo->findAll() as $shoppingCart) {
            $repo->delete($shoppingCart);
        }
    }

    public function cleanProductRepository(): void
    {
        /** @var OrderedProductRepositoryInterface $repo */
        $repo = $this->getModule('Symfony')->grabService(OrderedProductRepositoryInterface::class);
        foreach ($repo->findAll() as $orderedProduct) {
            $repo->delete($orderedProduct);
        }
    }

    public function generateShoppingCart(): CartId
    {
        /** @var ShoppingCartRepositoryInterface $repo */
        $repo = $this->getModule('Symfony')->grabService(ShoppingCartRepositoryInterface::class);

        $cart = new ShoppingCart();
        $cart->setCartId(new CartId(Uuid::uuid4()->toString()));
        $repo->insert($cart);
        return $cart->getCartId();
    }

    public function addProductsToCart(CartId $cartId, array $products): void
    {
        /** @var OrderedProductRepositoryInterface $repo */
        $repo = $this->getModule('Symfony')->grabService(OrderedProductRepositoryInterface::class);
        foreach ($products as $product) {
            $orderedProduct = new OrderedProduct();
            $orderedProduct->setCartId($cartId);
            $orderedProduct->setProductId($product['product_id']);
            $orderedProduct->setAmount($product['amount']);
            $repo->insert($orderedProduct);
        }
    }

    public function checkIfRepoHasCart(?CartId $cartId = null): bool
    {
        /** @var ShoppingCartRepositoryInterface $repo */
        $repo = $this->getModule('Symfony')->grabService(ShoppingCartRepositoryInterface::class);

        $cart = $repo->findAll();
        if ($cartId) {
            $cart = $repo->findOneBy(['cartId'=>$cartId]);
        }
        return !empty($cart);
    }

    public function checkIfRepoHasAnyProducts(?CartId $cartId = null): bool
    {
        /** @var OrderedProductRepositoryInterface $repo */
        $repo = $this->getModule('Symfony')->grabService(OrderedProductRepositoryInterface::class);
        $products = $repo->findAll();
        if ($cartId) {
            $products = $repo->findBy(['cartId'=>$cartId]);
        }
        return !empty($products);
    }

    public function getAmountOfProduct(CartId $cartId, ProductId $productId): ?int
    {
        /** @var OrderedProductRepositoryInterface $repo */
        $repo = $this->getModule('Symfony')->grabService(OrderedProductRepositoryInterface::class);
        $product = $repo->findOneBy(['cartId'=>$cartId,'productId'=>$productId]);
        if (!$product) {
            return null;
        }
        return $product->getAmount()->getValue();
    }
}
