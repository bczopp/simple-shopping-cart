<?php


namespace Bczopp\SimpleShoppingCart\Tests\api;

use Bczopp\SimpleShoppingCart\ValueObject\Amount;
use Bczopp\SimpleShoppingCart\ValueObject\ProductId;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

class RemoveOrderedProductCest
{
    public function _before(\ApiTester $I)
    {
        $I->cleanCartRepository();
        $I->cleanProductRepository();
    }

    public function failOnWrongIdFormat(\ApiTester $I)
    {
        $I->sendDelete('/api/v1/carts/someId/products/productId');
        $I->seeResponseCodeIs(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function failOnUnknownId(\ApiTester $I)
    {
        $id=Uuid::uuid4()->toString();
        $I->sendDelete('/api/v1/carts/'.$id.'/products/productId');
        $I->seeResponseCodeIs(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function successOnNonExistingProduct(\ApiTester $I)
    {
        $cartId = $I->generateShoppingCart();
        $I->sendDelete('/api/v1/carts/'.$cartId->getValue().'/products/productId');
        $I->seeResponseCodeIs(Response::HTTP_OK);
    }

    public function successOnExistingProduct(\ApiTester $I)
    {
        $cartId = $I->generateShoppingCart();
        $productId = new ProductId('testId');
        $amount = new Amount(1);
        $products = [
            ['product_id' => $productId, 'amount' => $amount]
        ];
        $I->addProductsToCart($cartId, $products);
        $I->assertTrue($I->checkIfRepoHasAnyProducts($cartId));
        $I->sendDelete('/api/v1/carts/'.$cartId->getValue().'/products/'.$productId->getValue());
        $I->seeResponseCodeIs(Response::HTTP_OK);

        $I->assertFalse($I->checkIfRepoHasAnyProducts($cartId));
    }
}
