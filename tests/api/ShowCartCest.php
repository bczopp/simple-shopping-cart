<?php


namespace Bczopp\SimpleShoppingCart\Tests\api;

use Bczopp\SimpleShoppingCart\DTO\Cart;
use Bczopp\SimpleShoppingCart\Entity\OrderedProduct;
use Bczopp\SimpleShoppingCart\Entity\ShoppingCart;
use Bczopp\SimpleShoppingCart\Services\Converter\CartConverter;
use Bczopp\SimpleShoppingCart\ValueObject\Amount;
use Bczopp\SimpleShoppingCart\ValueObject\CartId;
use Bczopp\SimpleShoppingCart\ValueObject\ProductId;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class ShowCartCest
{
    public function _before(\ApiTester $I)
    {
        $I->cleanCartRepository();
        $I->cleanProductRepository();
    }

    public function failOnWrongIdFormat(\ApiTester $I)
    {
        $I->sendGet('/api/v1/carts/someId');
        $I->seeResponseCodeIs(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function failOnUnknownId(\ApiTester $I)
    {
        $id=Uuid::uuid4()->toString();
        $I->sendGet('/api/v1/carts/'.$id);
        $I->seeResponseCodeIs(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function successCartWithoutProducts(\ApiTester $I)
    {
        $cartId = $I->generateShoppingCart();
        $I->sendGet('/api/v1/carts/'.$cartId->getValue());
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $I->assertIsArray($response);
        $I->assertEquals($this->generateExpected($cartId, []), $response);
    }

    public function successCartWithProducts(\ApiTester $I)
    {
        $cartId = $I->generateShoppingCart();
        $products = [
            ['product_id'=>new ProductId('test1'),'amount'=>new Amount(1)],
            ['product_id'=>new ProductId('test2'),'amount'=>new Amount(2)]
        ];
        $I->addProductsToCart($cartId, $products);
        $I->sendGet('/api/v1/carts/'.$cartId->getValue());
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $I->assertIsArray($response);
        $I->assertEquals($this->generateExpected($cartId, $products), $response);
    }

    private function generateExpected(CartId $cartId, array $products)
    {
        $meta = ['cart_id'=>$cartId->getValue()];
        $items = array_map(
            fn($product) => [
                'product_id' => $product['product_id']->getValue(),
                'amount' => $product['amount']->getValue()
            ],
            $products
        );
        return [
            'meta_data' => $meta,
            'items' => $items
        ];
    }

}
