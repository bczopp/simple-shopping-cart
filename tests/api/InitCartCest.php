<?php


namespace Bczopp\SimpleShoppingCart\Tests\api;

use Assert\Assertion;
use Symfony\Component\HttpFoundation\Response;

class InitCartCest
{
    public function _before(\ApiTester $I)
    {
        $I->cleanCartRepository();
        $I->cleanProductRepository();
    }

    public function init(\ApiTester $I)
    {
        $url = '/api/v1/carts';
        $I->sendPost($url);
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        Assertion::uuid($response);
    }
}
