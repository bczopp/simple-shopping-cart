<?php


namespace Bczopp\SimpleShoppingCart\Tests\api;

use Bczopp\SimpleShoppingCart\ValueObject\Amount;
use Bczopp\SimpleShoppingCart\ValueObject\ProductId;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

class EditOrderedProductCest
{
    public function _before(\ApiTester $I)
    {
        $I->cleanCartRepository();
        $I->cleanProductRepository();
    }

    public function failOnWrongIdFormat(\ApiTester $I)
    {
        $I->sendPut('/api/v1/carts/someId/products/productId/amount/1');
        $I->seeResponseCodeIs(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function failOnUnknownId(\ApiTester $I)
    {
        $id=Uuid::uuid4()->toString();
        $I->sendPut('/api/v1/carts/'.$id.'/products/productId/amount/1');
        $I->seeResponseCodeIs(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function failOnNonExistingProduct(\ApiTester $I)
    {
        $id=$I->generateShoppingCart();
        $I->sendPut('/api/v1/carts/'.$id->getValue().'/products/productId/amount/1');
        $I->seeResponseCodeIs(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function successOnExistingProduct(\ApiTester $I)
    {
        $cartId = $I->generateShoppingCart();
        $productId = new ProductId('test');
        $amount = new Amount(1);
        $I->addProductsToCart($cartId, [['product_id'=>$productId,'amount'=>$amount]]);

        $newAmount = new Amount(3);
        $I->sendPut('/api/v1/carts/'.$cartId->getValue().'/products/'.$productId->getValue().'/amount/'.$newAmount->getValue());
        $I->seeResponseCodeIs(Response::HTTP_OK);


        # so klaptt es (nicht schön)
        $I->sendGet('/api/v1/carts/'.$cartId->getValue());
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals($newAmount->getValue(), $response['items'][0]['amount']);

        #so nicht ...
//        $I->assertEquals($newAmount->getValue(), $I->getAmountOfProduct($cartId, $productId));
    }
}
