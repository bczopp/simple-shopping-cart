<?php


namespace Bczopp\SimpleShoppingCart\Tests\api;

use Bczopp\SimpleShoppingCart\ValueObject\Amount;
use Bczopp\SimpleShoppingCart\ValueObject\ProductId;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

class DeleteCartCest
{
    public function _before(\ApiTester $I)
    {
        $I->cleanCartRepository();
        $I->cleanProductRepository();
    }

    public function successOnWrongIdFormat(\ApiTester $I)
    {
        $I->sendDelete('/api/v1/carts/someId');
        $I->seeResponseCodeIs(Response::HTTP_OK);
    }

    public function successOnUnknownId(\ApiTester $I)
    {
        $id=Uuid::uuid4()->toString();
        $I->sendDelete('/api/v1/carts/'.$id);
        $I->seeResponseCodeIs(Response::HTTP_OK);
    }

    public function success(\ApiTester $I)
    {
        $cartId = $I->generateShoppingCart();
        $products = [
            ['product_id'=>new ProductId('test1'),'amount'=>new Amount(1)],
            ['product_id'=>new ProductId('test2'),'amount'=>new Amount(2)]
        ];
        $I->addProductsToCart($cartId, $products);

        $I->assertTrue($I->checkIfRepoHasCart($cartId));
        $I->assertTrue($I->checkIfRepoHasAnyProducts($cartId));

        $I->sendDelete('api/v1/carts/'.$cartId->getValue());
        $I->assertFalse($I->checkIfRepoHasCart($cartId));
        $I->assertFalse($I->checkIfRepoHasAnyProducts($cartId));
    }
}
