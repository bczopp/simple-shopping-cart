<?php


namespace Bczopp\SimpleShoppingCart\Tests\api;

use Bczopp\SimpleShoppingCart\ValueObject\Amount;
use Bczopp\SimpleShoppingCart\ValueObject\ProductId;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

class AddProductCest
{
    public function _before(\ApiTester $I)
    {
        $I->cleanCartRepository();
        $I->cleanProductRepository();
    }

    public function failOnWrongIdFormat(\ApiTester $I)
    {
        $I->sendPost('/api/v1/carts/someId/products/productId/amount/1');
        $I->seeResponseCodeIs(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function failOnUnknownId(\ApiTester $I)
    {
        $id=Uuid::uuid4()->toString();
        $I->sendPost('/api/v1/carts/'.$id.'/products/productId/amount/1');
        $I->seeResponseCodeIs(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function addNew(\ApiTester $I)
    {
        $cartId = $I->generateShoppingCart();
        $productId = new ProductId('test');
        $amount = new Amount(1);
        $I->assertFalse($I->checkIfRepoHasAnyProducts($cartId));

        $I->sendPost('/api/v1/carts/'.$cartId->getValue().'/products/'.$productId->getValue().'/amount/'.$amount->getValue());
        $I->seeResponseCodeIs(Response::HTTP_OK);

        $I->assertTrue($I->checkIfRepoHasAnyProducts($cartId));
    }

    public function addToExisting(\ApiTester $I)
    {
        $cartId = $I->generateShoppingCart();
        $productId = new ProductId('test');
        $amount = new Amount(1);
        $I->addProductsToCart($cartId, [['product_id'=>$productId,'amount'=>$amount]]);

        $I->sendPost('/api/v1/carts/'.$cartId->getValue().'/products/'.$productId->getValue().'/amount/'.$amount->getValue());
        $I->seeResponseCodeIs(Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
